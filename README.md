## Alpine Linux: help

## TL;DR

You are in trouble? You can reach us at **help at alpinelinux.org**.

Note: this group does **not** offer technical help or helps with
technical development.

## Introduction


Alpine Help is a group of volunteers working to help make Alpine Linux
a welcoming place
for contributors to collaborate. We're not working alone here - we
expect all of Alpine Linux' contributors to be part of that. But we're here
to help as/when/if things are not working so well.

(this introduction was strongly inspired by
[Debian's Community Team
Introduction](https://wiki.debian.org/Teams/Community))

## Status of Alpine Help

As of 2022-02-07, this is a new project established to take the
requests from within our community serious when it comes to violations
of the Code of Conduct.

## Help the helpers

In case you feel you can contribute to make Alpine Linux a place where
people feel comfortable developing together and you want to join the
team, also feel free to drop us a message.
